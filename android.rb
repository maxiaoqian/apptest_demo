require 'rubygems'
require 'appium_lib'
require 'rspec'

# 测试开源中国app
opts = {
        caps: {
          platformName: :android,
          #platformVersion: '',
          deviceName: '127.0.0.1:62001',
          app: File.expand_path('../apks/net.oschina.app.apk', __FILE__),
          appActivity: 'net.oschina.app.LaunchActivity',
          appPackage: 'net.oschina.app',
          automation_name: 'UiAutomator2'
        },
        appium_lib: {
          # set timeouts to avoid the driver close too soon, seconds
          wait_timeout: 30,
          # appium 默认配置
          appium_port: 4723,
          server_url: 'http://127.0.0.1:4723/wd/hub',
          debug: false,
          sauce_username: nil,
          sauce_access_key: nil
        }
      }

puts '初始化Appium Driver，操作服务器上的 App...'
# Initial a driver
driver = Appium::Driver.new(opts)
driver.start_driver
sleep(5)
# load the appium own methods
Appium.promote_appium_methods self.class


puts '先点击订阅频道，再点击我的，再点击头像进入登录页面...'
dingyue_btn = id('net.oschina.app:id/tv_title')
dingyue_btn.click
own = id('net.oschina.app:id/nav_item_me')
own.click
portrait = id('net.oschina.app:id/iv_portrait')
portrait.click
sleep(5)


puts '输入注册邮箱和密码，登录...'
login_username = id('net.oschina.app:id/et_login_username')
login_username.type('maxiaoqian199202@foxmail.com')
login_pwd = id('net.oschina.app:id/et_login_pwd')
login_pwd.type('oschina123')
submit_btn = id('net.oschina.app:id/bt_login_submit')
submit_btn.click
sleep(5)


puts '点击底部加号按钮，选择发动弹，进入动弹编辑页面...'
nav_btn = id('net.oschina.app:id/nav_item_tweet_pub')
nav_btn.click
dongtan_btn = id('net.oschina.app:id/ll_pub_tweet')
dongtan_btn.click
sleep(3)


puts '填写动弹内容，然后点击发送...'
dongtan_content = id('net.oschina.app:id/edit_content')
dongtan_content.type('test')
send_btn = id('net.oschina.app:id/icon_send')
send_btn.click
sleep(3)




