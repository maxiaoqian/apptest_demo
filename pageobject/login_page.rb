class LoginPage
  # 进入登录页面
  def self.goto_loginPage
    puts '先点击订阅频道，再点击我的，再点击头像进入登录页面...'
    id('net.oschina.app:id/tv_title').click
    own = id('net.oschina.app:id/nav_item_me')
    own.click
    portrait = id('net.oschina.app:id/iv_portrait')
    portrait.click
    sleep(3)
  end

  # 登录
  def self.login(email, password)
    puts '登录...'
    login_username = id('net.oschina.app:id/et_login_username')
    login_pwd = id('net.oschina.app:id/et_login_pwd')
    submit_btn = id('net.oschina.app:id/bt_login_submit')

    login_username.type("#{email}")
    login_pwd.type("#{password}")
    submit_btn.click
    sleep(5)
  end

end